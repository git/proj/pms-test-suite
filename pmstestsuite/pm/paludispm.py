#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

"""
>>> pm = PaludisPM()
>>> pm.get_repository('gentoo') # doctest: +ELLIPSIS
<pmstestsuite.repository.EbuildRepository object at ...>
"""

import os, os.path, subprocess
from gentoopm.paludispm import PaludisPM as _PaludisPM

from . import PackageManager

class PaludisPM(_PaludisPM, PackageManager):
	"""
	A class implementing the interfaces to the Paludis PM.

	Requires Paludis with C{USE=python}.
	"""
	name = 'paludis'

	cave_path = '/usr/bin/cave'
	common_cave_opts = ['--lazy', '--preserve-world', '--execute']
	requires_manifests = True

	def __init__(self, *args, **kwargs):
		PackageManager.__init__(self)
		_PaludisPM.__init__(self, *args, **kwargs)

	def _spawn_cave(self, cpvs, action, opts = []):
		return subprocess.Popen(['cave', '--log-level', 'warning', action]
				+ self.common_cave_opts + opts + self.pm_options
				+ ['=%s' % cpv for cpv in cpvs])

	def _spawn_merge(self, cpvs):
		return self._spawn_cave(cpvs, 'resolve', ['--continue-on-failure', 'if-satisfied'])

	def _spawn_unmerge(self, cpvs):
		return self._spawn_cave(cpvs, 'uninstall')

	def append_repository(self, repo):
		raise NotImplementedError('PaludisPM does not support adding repositories yet.')

	def remanifest(self, paths):
		for path in paths:
			tmp, pn = os.path.split(path)
			repo_path, cat = os.path.split(tmp)
			repo_name = self.repositories[repo_path].name

			subprocess.check_call(['cave', '--log-level', 'silent',
				'digest', '%s/%s' % (cat, pn), repo_name])
