#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

"""
>>> pm = PortagePM()
>>> pm.get_repository('gentoo') # doctest: +ELLIPSIS
<pmstestsuite.repository.EbuildRepository object at ...>
"""

import copy, os, subprocess
from gentoopm.portagepm import PortagePM as _PortagePM

from . import PackageManager

class PortagePM(_PortagePM, PackageManager):
	""" A class implementing the interfaces to the Portage PM. """
	name = 'portage'

	common_emerge_opts = ['--ask', 'n', '--keep-going', '--oneshot',
			'--quiet-unmerge-warn', '--fail-clean', 'y']
	requires_manifests = False

	repo_paths = []

	def __init__(self, *args, **kwargs):
		PackageManager.__init__(self)
		_PortagePM.__init__(self, *args, **kwargs)

	def remanifest(self, paths):
		startdir = os.getcwd()
		for path in paths:
			os.chdir(path)
			subprocess.check_call(['repoman', 'manifest'])
		os.chdir(startdir)

	def spawn_emerge(self, cpvs, opts = []):
		env = copy.copy(os.environ)
		env['FEATURES'] = '-buildpkg -strict'
		env['CLEAN_DELAY'] = '0'
		if self.repo_paths:
			env['PORTDIR_OVERLAY'] = ' '.join(self.repo_paths)

		return subprocess.Popen(['emerge']
				+ self.common_emerge_opts + opts + self.pm_options
				+ ['=%s' % cpv for cpv in cpvs],
				env = env)

	def _spawn_merge(self, cpvs):
		return self.spawn_emerge(cpvs)

	def _spawn_unmerge(self, cpvs):
		return self.spawn_emerge(cpvs, ['--unmerge'])
