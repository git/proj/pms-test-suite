#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

"""
>>> pm = PkgCorePM()
>>> pm.get_repository('gentoo') # doctest: +ELLIPSIS
<pmstestsuite.repository.EbuildRepository object at ...>
"""

import subprocess
from gentoopm.pkgcorepm import PkgCorePM as _PkgCorePM

from . import PackageManager

class PkgCorePM(_PkgCorePM, PackageManager):
	"""
	A class implementing the interfaces to the pkgcore PM.
	"""
	name = 'pkgcore'

	common_pmerge_opts = ['--oneshot']
	requires_manifests = False

	def __init__(self, *args, **kwargs):
		PackageManager.__init__(self)
		_PkgCorePM.__init__(self, *args, **kwargs)

	@property
	def package_limit(self):
		return 1

	@package_limit.setter
	def package_limit(self, val):
		pass

	def spawn_pmerge(self, cpvs, opts = []):
		return subprocess.Popen(['pmerge']
				+ self.common_pmerge_opts + opts + self.pm_options
				+ ['=%s' % cpv for cpv in cpvs])

	def _spawn_merge(self, cpvs):
		return self.spawn_pmerge(cpvs)

	def _spawn_unmerge(self, cpvs):
		return self.spawn_pmerge(cpvs, ['--unmerge'])

	def append_repository(self, repo):
		raise NotImplementedError('PkgCorePM does not support adding repositories yet.')

	def get_repository(self, repo):
		r = PackageManager.get_repository(self, repo)
		self.common_pmerge_opts.extend(['--add-config',
			r.path, 'allow_missing_manifests', 'True'])
		return r

	# pkgcore can't do Manifests
	def remanifest(self, *args):
		raise NotImplementedError()
