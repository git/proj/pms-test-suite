#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from traceback import format_exc

from gentoopm.util import ABCObject, BoolCompat
from abc import abstractmethod, abstractproperty

class TestResult(BoolCompat):
	""" Test result container. """

	_SUCCESS = 0
	_FAILURE = 1
	_EXCEPT = 2

	def __init__(self, t, *args):
		"""
		Instantiate test results by checking the test results.

		@param t: test to check
		@type t: L{TestCase}
		@param args: args to pass to check_result()
		"""

		try:
			t.check_result(*args)
		except AssertionError:
			self._res = self._FAILURE
		except Exception as e:
			self._res = self._EXCEPT
			self._exc = format_exc()
		else:
			self._res = self._SUCCESS
		self._assert = t.pop_assertions()

	def __bool__(self):
		return self._res == self._SUCCESS

	@property
	def assertions(self):
		return self._assert

	@property
	def exception(self):
		if self._res == self._EXCEPT:
			return self._exc
		return None

	@property
	def undefined(self):
		return not filter(lambda a: not a.undefined, self.assertions)

class OutputModule(ABCObject):
	""" A module handling test results output. """

	@abstractproperty
	def name(self):
		"""
		Output module name.

		@type: string
		"""
		pass

	def __init__(self, output_file = None):
		pass

	@abstractmethod
	def __call__(self, results, verbose = False):
		"""
		Output the test results.

		@param results: test result dict
		@type results: dict(L{PackageManager} -> dict(L{TestCase} -> L{TestResult}))
		@param verbose: whether to output the results verbosely
		@type verbose: bool
		@return: whether all of the tests succeeded
		@rtype: bool
		"""
		pass

def get_output_modules():
	"""
	Return the list of supported output modules.

	@return: supported output modules
	@rtype: L{OutputModule}
	"""

	from pmstestsuite.output.cli import CLIOutput
	from pmstestsuite.output.html import HTMLOutput
	from pmstestsuite.output.wiki import WikiOutput

	return (CLIOutput, HTMLOutput, WikiOutput)
