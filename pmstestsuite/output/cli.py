#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from . import OutputModule

class CLIOutput(OutputModule):
	""" Command-line output module. """

	name = 'cli'

	def __call__(self, allresults, verbose = False):
		ret = True
		for pm, results in allresults.items():
			failed = filter(lambda tr: not tr[1], results.items())
			if not failed and not verbose:
				print('[%s] %d tests completed successfully.'
						% (pm.name, len(results)))
			else:
				print('[%s] %d of %d tests completed successfully, %d failed:'
						% (pm.name, len(results) - len(failed),
							len(results), len(failed)))
				tl = failed if not verbose else results.items()
				for t, r in tl:
					print('- %s [%s%s]' % (t,
						'OK' if r else 'FAILED',
						'/UNDEF' if r.undefined else ''))
					for a in r.assertions:
						print('-> %s: %s [%s%s]' % (a.name, str(a),
								'OK' if a else 'FAILED',
								'/UNDEF' if a.undefined else ''))
			ret &= not bool(failed)

		return ret
