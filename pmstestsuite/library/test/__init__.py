#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from pmstestsuite.library import TestLibrary

class ExampleLibrary(TestLibrary):
	"""
	Absolutely random TestLibrary subclass to test it.
	"""

	test_names=[
		'random_test.RandomExampleTest'
	]
