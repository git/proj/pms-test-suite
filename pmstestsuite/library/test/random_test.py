#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from pmstestsuite.library.case import EbuildTestCase

class RandomExampleTest(EbuildTestCase):
	""" An absolutely random test. """

	supported_eapis = ((0,), (2, 4))
	expect_failure = True

	phase_funcs = {
		'pkg_setup': ['die "Random failure"']
	}
