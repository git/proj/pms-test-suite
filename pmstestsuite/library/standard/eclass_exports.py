#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from abc import abstractproperty

from pmstestsuite.library.eclass_case import EclassTestCase

class ExportedPhaseFunctionTest(EclassTestCase):
	""" Check whether EXPORTED_FUNCTIONS are used. """

	@property
	def eclass_contents(self):
		return '''
EXPORT_FUNCTIONS src_unpack

%s_src_unpack() {
	pms-test_dbus_append_result 'working'
}
''' % self.pn

	def check_dbus_result(self, output, pm):
		self.assertEqual(output[0], 'working', 'EXPORT_FUNCTIONS')
