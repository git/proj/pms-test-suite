#	vim:fileencoding=utf-8
# (c) 2011-2012 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from pmstestsuite.library.case import EbuildTestCase

class InheritedVariableTest(EbuildTestCase):
	""" INHERITED variable definition test. """

	phase_funcs = {
		'src_compile': [
			'pms-test_dbus_append_result "${INHERITED}"'
		]
	}

	def check_dbus_result(self, output, pm):
		inherits = output[0].split()
		self.assertContains('pms-test', inherits, 'INHERITED')

class RDependFallbackTest(EbuildTestCase):
	""" Test whether RDEPEND=${DEPEND} fallback works as expected. """

	supported_eapis = (range(0, 4), (4,))
	ebuild_vars = {
		# that one shall be pretty portable
		'DEPEND': 'virtual/libc'
	}

	def check_dbus_result(self, output, pm):
		class DepMatcher(object):
			def __eq__(self, other):
				return other.key == str(self)

			def __str__(self):
				return 'virtual/libc'

			def __repr__(self):
				return repr(str(self))

		rdep = pm.installed[self.atom(pm)].run_dependencies
		mydep = DepMatcher()

		# in EAPI 4, expect empty RDEPEND
		# in older EAPIs, expect == DEPEND
		if self.eapi == 4:
			self.assertEqual(tuple(rdep), (), 'RDEPEND')
		else:
			self.assertContains(mydep, rdep, 'RDEPEND')

class DefinedPhasesTest(EbuildTestCase):
	""" Test whether DEFINED_PHASES are declared in EAPI 4. """

	supported_eapis = ((4,),)
	phase_funcs = {
		'pkg_setup': [
			':'
		]
	}

	def check_dbus_result(self, output, pm):
		phases = pm.installed[self.atom(pm)].defined_phases
		self.assertEqual(tuple(phases), ('setup',), 'DEFINED_PHASES')
