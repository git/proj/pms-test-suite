#	vim:fileencoding=utf-8
# (c) 2011-2012 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

import os.path

from pmstestsuite.library.case import EbuildTestCase
from .ext_cases import FetchingEbuildTestCase

class AATest(FetchingEbuildTestCase):
	""" Test whether AA is declared. """

	supported_eapis = (range(0, 4), (4,))
	ebuild_vars = {
		'IUSE': 'pms_tests_magical_hidden_use' # XXX?
	}
	phase_funcs = {
		'src_unpack': [
			'pms-test_dbus_append_result "${AA}"'
		]
	}

	def __init__(self, *args, **kwargs):
		FetchingEbuildTestCase.__init__(self, *args, **kwargs)
		self._expect_fn = os.path.split(self.ebuild_vars['SRC_URI'])[1]
		self.ebuild_vars['SRC_URI'] = 'pms_tests_magical_hidden_use? ( %s )' \
				% self.ebuild_vars['SRC_URI']

	def check_dbus_result(self, output, pm):
		if self.eapi < 4:
			expect = self._expect_fn
		else:
			expect = ''

		self.assertEqual(output[0], expect, '${AA}')

class KVTest(EbuildTestCase):
	""" Test whether KV is declared. """

	supported_eapis = (range(0, 4), (4,))
	phase_funcs = {
		'src_compile': [
			'pms-test_dbus_append_result "${KV}"'
		]
	}

	def check_dbus_result(self, output, pm):
		func = self.assertEqual if self.eapi == 4 \
				else self.assertNotEqual
		func(output[0], '', '${KV}')
