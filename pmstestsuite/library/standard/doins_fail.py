#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from pmstestsuite.library.case import EbuildTestCase

class DoInsFailureTest(EbuildTestCase):
	""" doins() failure handling test. """

	supported_eapis = (range(0, 4), (4,))

	phase_funcs = {
		'src_install': [
			'doins testzor',
			'[[ ${?} -ne 0 ]] || die "doins exitcode = 0!"'
		]
	}

	def __init__(self, *args, **kwargs):
		EbuildTestCase.__init__(self, *args, **kwargs)
		self.expect_failure = (self.eapi == 4)
