#	vim:fileencoding=utf-8
# (c) 2011-2012 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

import os.path

from pmstestsuite.library import TestLibrary

class StandardTestLibrary(TestLibrary):
	"""
	Standard test library for the PMS Test Suite.
	"""

	test_names=[
		'banned_commands.DoHardCommandTest',
		'banned_commands.DoSedCommandTest',
		'depend.DependTest',
		'depend.FailingDependTest',
		'depend.FailingRDependTest',
		'depend.PDependTest',
		'depend.RDependTest',
		'deprecated_vars.AATest',
		'deprecated_vars.KVTest',
		'doins_fail.DoInsFailureTest',
		'eclass_depend.DependInheritanceTest',
		'eclass_depend.PDependInheritanceTest',
		'eclass_depend.RDependInheritanceTest',
		'eclass_exports.ExportedPhaseFunctionTest',
		'eclass_metadata.EclassDefinedPhasesTest',
		'eclass_metadata.IUseInheritanceTest',
		'phase_function_order.PhaseFunctionOrderTest',
		'special_vars.DefinedPhasesTest',
		'special_vars.InheritedVariableTest',
		'special_vars.RDependFallbackTest',
		'variable_scope.VariableScopeTest',
		'workdir_fallback.WorkdirFallbackTest'
	]
