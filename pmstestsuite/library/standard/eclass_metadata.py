#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from abc import abstractproperty

from pmstestsuite.library.eclass_case import EclassTestCase

class VariableInheritanceTest(EclassTestCase):
	""" Eclass variable inheritance test. """

	@abstractproperty
	def var_name(self):
		""" Variable name to test. """
		pass

	@abstractproperty
	def var_eclass_value(self):
		""" Inherited value to test. """
		pass

	@abstractproperty
	def var_ebuild_value(self):
		""" Ebuild variable value to test. """
		pass

	@property
	def eclass_contents(self):
		return '%s=%s\n' % (self.var_name, repr(self.var_eclass_value))

	def __init__(self, *args, **kwargs):
		EclassTestCase.__init__(self, *args, **kwargs)
		self.ebuild_vars[self.var_name] = self.var_ebuild_value

class IUseInheritanceTest(VariableInheritanceTest):
	""" IUSE variable inheritance test. """

	var_name = 'IUSE'
	var_eclass_value = 'foo'
	var_ebuild_value = 'bar'

	def check_dbus_result(self, output, pm):
		spl = pm.installed[self.atom(pm)].use
		self.assertContains(self.var_eclass_value, spl, 'IUSE (eclass)')
		self.assertContains(self.var_ebuild_value, spl, 'IUSE (ebuild)')

# XXX: REQUIRED_USE

class EclassDefinedPhasesTest(EclassTestCase):
	""" Check whether eclasses set DEFINED_PHASES as well. """

	supported_eapis = ((4,),)
	phase_funcs = {
		'src_install': [
			':'
		]
	}

	@property
	def eclass_contents(self):
		return '''
EXPORT_FUNCTIONS src_compile

%s_src_compile() {
	:
}
''' % self.pn

	def check_dbus_result(self, output, pm):
		phases = pm.installed[self.atom(pm)].defined_phases
		self.assertContains('compile', phases, 'DEFINED_PHASES (eclass)')
		self.assertContains('install', phases, 'DEFINED_PHASES (ebuild)')
