#	vim:fileencoding=utf-8
# (c) 2011-2012 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

import re

from pmstestsuite.library.case import EbuildTestCase

class VariableScopeTest(EbuildTestCase):
	""" A test for scoping of variables. """

	ebuild_vars = {
		'GLOBAL_TEST': 'foo',
		'UNSET_GLOBAL': 'foo'
	}
	phase_funcs = {
		'src_unpack': [
			'unset LOCAL_TEST'
		],
		'src_compile': [
			'GLOBAL_TEST=bar',
			'unset UNSET_GLOBAL',
			'DEFAULT_TEST=foo',
			'export EXPORT_TEST=foo',
			'local LOCAL_TEST=bar'
		],
		'src_install': [
			'pms-test_dbus_append_result "$(declare -p %s)"' % var
				for var in ('GLOBAL_TEST', 'UNSET_GLOBAL', 'DEFAULT_TEST',
					'EXPORT_TEST', 'LOCAL_TEST')
		]
	}

	declare_re = re.compile(r'^declare -([-x]) ([A-Z_]+)="([a-z]+)"$')
	def check_dbus_result(self, output, pm):
		class RegExpMatcher(object):
			""" A matcher using a regular expression. """

			def __init__(self, regexp, name):
				self._re = re.compile(regexp)
				self._re_str = regexp
				self._name = name

			def __eq__(self, other):
				return bool(self._re.match(other))

			def __repr__(self):
				return 're(%s)' % repr(self._re_str.strip('^$'))

			@property
			def name(self):
				return self._name

		matches = [
				# GLOBAL can retain its value or be reset
				RegExpMatcher(r'^declare -[-x] GLOBAL_TEST="(foo|bar)"$',
					'global variable'),
				# UNSET_GLOBAL can remain unset or be reset
				RegExpMatcher(r'^(|declare -[-x] GLOBAL_TEST="foo")$',
					'unset global variable'),
				# DEFAULT must retain its value
				RegExpMatcher(r'^declare -[-x] DEFAULT_TEST="foo"$',
					'simple variable'),
				# EXPORT must retain its value and be exported
				RegExpMatcher(r'^declare -x EXPORT_TEST="foo"$',
					'exported variable'),
				# LOCAL must be forgotten
				RegExpMatcher(r'^$', 'local variable')
		]

		for var, regexp in zip(output, matches):
			self.assertEqual(var, regexp, regexp.name)
