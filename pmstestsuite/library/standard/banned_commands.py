#	vim:fileencoding=utf-8
# (c) 2011-2012 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from pmstestsuite.library.case import EbuildTestCase

class BannedCommandTest(EbuildTestCase):
	supported_eapis = (range(0, 4), (4,))

	def __init__(self, *args, **kwargs):
		EbuildTestCase.__init__(self, *args, **kwargs)
		self._path = '/tmp/pms-test-suite-%d' % id(self)
		self.phase_funcs['src_install'].extend([
			'{ echo > foo; } || die',
			'insinto %s' % self._path,
			'doins foo || die'
		])
		self.expect_failure = (self.eapi == 4)

class DoHardCommandTest(BannedCommandTest):
	""" Test whether dohard command is actually banned. """

	def __init__(self, *args, **kwargs):
		BannedCommandTest.__init__(self, *args, **kwargs)
		self.phase_funcs['src_install'].extend([
				'dohard %s/foo %s/bar || die' % (self._path, self._path),
				'[[ "${D}"%s/foo -ef "${D}"%s/bar ]]' % (self._path, self._path),
				'pms-test_dbus_append_result ${?}'])
		self.phase_funcs['pkg_postinst'].extend([
				'[[ "${ROOT}"%s/foo -ef "${ROOT}"%s/bar ]]' % (self._path, self._path),
				'pms-test_dbus_append_result ${?}'])

	def check_dbus_result(self, output, pm):
		try:
			res = True if output[0] == '0' else False
		except IndexError:
			res = None

		exc = None
		if self.eapi < 4:
			try:
				self.assertTrue(res, 'hardlink created')
			except AssertionError as e:
				exc = e
			try:
				self.assertTrue(output[1] == '0', 'hardlink preserved after merge',
						undefined = True)
			except IndexError:
				pass
		elif res is not None or exc:
			self.assertFalse(res, 'hardlink created',
					undefined = True)
			try:
				self.assertFalse(output[1] == '0', 'hardlink preserved after merge',
						undefined = True)
			except IndexError:
				pass

		if exc:
			raise exc

class DoSedCommandTest(BannedCommandTest):
	""" Test whether dosed command is actually banned. """

	def __init__(self, *args, **kwargs):
		BannedCommandTest.__init__(self, *args, **kwargs)
		self.phase_funcs['src_install'].extend([
				"dosed -e '$i SED WORKED' %s/foo || die" % self._path,
				'pms-test_dbus_append_result "$(cat "${D}"%s/foo)"' % self._path])

	def check_dbus_result(self, output, pm):
		try:
			res = output[0].strip()
		except IndexError:
			res = None

		if self.eapi < 4:
			self.assertEqual(res, 'SED WORKED', 'dosed result')
		elif res is not None:
			self.assertNotEqual(res, 'SED WORKED', 'dosed result',
					undefined = True)
