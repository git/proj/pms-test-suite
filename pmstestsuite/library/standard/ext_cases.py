#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from pmstestsuite.library.case import EbuildTestCase

class FetchingEbuildTestCase(EbuildTestCase):
	"""
	A EbuildTestCase variant with a standarized way of fetching
	sources -- to decrease {${DISTDIR}} pollution.
	"""

	def __init__(self, *args, **kwargs):
		EbuildTestCase.__init__(self, *args, **kwargs)
		val = 'http://dev.gentoo.org/~mgorny/pmsts-temp.bin'
		if 'SRC_URI' in self.ebuild_vars:
			val = '%s %s' % (self.ebuild_vars['SRC_URI'], val)
		self.ebuild_vars['SRC_URI'] = val
