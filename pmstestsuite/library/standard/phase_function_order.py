#	vim:fileencoding=utf-8
# (c) 2011-2012 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from pmstestsuite.library.case import EbuildTestCase

class PhaseFunctionOrderTest(EbuildTestCase):
	""" Phase function execution order test. """

	supported_eapis = ((0, 1), (2, 3), (4,))

	def __init__(self, *args, **kwargs):
		EbuildTestCase.__init__(self, *args, **kwargs)
		for k, lines in self.phase_funcs.items():
			lines.append('pms-test_dbus_append_result %s' % k)

	def check_dbus_result(self, output, pm):
		expect = []

		if self.eapi >= 4:
			expect.append('pkg_pretend')
		expect.extend(['pkg_setup', 'src_unpack'])
		if self.eapi >= 2:
			expect.extend(['src_prepare', 'src_configure'])
		expect.extend(['src_compile', 'src_install', 'pkg_preinst', 'pkg_postinst'])

		self.assertEqual(output, expect, 'Executed phases')
