#	vim:fileencoding=utf-8
# (c) 2011-2012 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from pmstestsuite.library.depend_case import EbuildDependencyTestCase
from .util import EbuildToucher, FailingEbuild

class DependTest(EbuildDependencyTestCase):
	""" DEPEND fulfilling test. """

	depend_classes = [EbuildToucher]

	def __init__(self, *args, **kwargs):
		EbuildDependencyTestCase.__init__(self, *args, **kwargs)
		self.phase_funcs['src_compile'].append(
			'pms-test-suite-%s || die' % self.dependant_ebuilds[0].pv
		)

class RDependTest(EbuildDependencyTestCase):
	""" RDEPEND fulfilling test. """

	rdepend_classes = [EbuildToucher]

	def __init__(self, *args, **kwargs):
		EbuildDependencyTestCase.__init__(self, *args, **kwargs)
		self.phase_funcs['pkg_postinst'].append(
			'pms-test-suite-%s || die' % self.dependant_ebuilds[0].pv
		)

class PDependTest(EbuildDependencyTestCase):
	""" PDEPEND fulfilling test. """

	pdepend_classes = [EbuildToucher]

	def __init__(self, *args, **kwargs):
		EbuildDependencyTestCase.__init__(self, *args, **kwargs)
		self.phase_funcs['pkg_postinst'].extend([
			'pms-test-suite-%s' % self.dependant_ebuilds[0].pv,
			'pms-test_dbus_append_result ${?}'
		])

	def check_dbus_result(self, output, pm):
		try:
			res = output[0] == '0'
		except IndexError:
			res = None
		self.assertFalse(res, 'PDEP merged before ebuild',
				undefined = True)

class FailingDependTest(EbuildDependencyTestCase):
	""" Unfulfilled DEPEND test. """

	depend_classes = [FailingEbuild]
	expect_failure = True

class FailingRDependTest(EbuildDependencyTestCase):
	""" Unfulfilled RDEPEND test. """

	depend_classes = [FailingEbuild]
	expect_failure = True
