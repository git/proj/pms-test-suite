#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from .ext_cases import FetchingEbuildTestCase

class WorkdirFallbackTest(FetchingEbuildTestCase):
	""" S=${WORKDIR} fallback test. """

	supported_eapis = (range(0, 4), (4,))

	# In order to disallow S=${WORKDIR} fallback, we need to:
	# 1) have something in ${A} (i.e. fetch something),
	# 2) have src_unpack() of some kind,
	# 3) have one of the further phase funcs.

	ebuild_vars = {
		'S': '${WORKDIR}/nonexistent'
	}
	phase_funcs = {
		'src_unpack': [
			'echo ${A}'
		],
		'src_compile': [
			':'
		]
	}

	def __init__(self, *args, **kwargs):
		FetchingEbuildTestCase.__init__(self, *args, **kwargs)
		self.expect_failure = (self.eapi == 4)
