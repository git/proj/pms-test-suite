#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from pmstestsuite.library.case import EbuildTestCase

class EbuildToucher(EbuildTestCase):
	""" Touched file installer (for dependency tests). """

	@property
	def pv(self):
		return id(self)

	def __init__(self, *args, **kwargs):
		EbuildTestCase.__init__(self, *args, **kwargs)
		self.phase_funcs['src_unpack'].append(
			'''cat >> mytrue.sh <<_EOF_
#!/bin/sh
true
_EOF_'''
		)
		self.phase_funcs['src_install'].append(
			'newbin mytrue.sh pms-test-suite-%s || die' % self.pv
		)

class FailingEbuild(EbuildTestCase):
	""" A failing ebuild. """

	phase_funcs = {
		'src_unpack': ['die']
	}
	expect_failure = True

	@property
	def pv(self):
		return id(self)
