#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from pmstestsuite.library.depend_case import EclassDependencyTestCase
from .util import EbuildToucher

class DependInheritanceTest(EclassDependencyTestCase):
	""" DEPEND variable inheritance test. """

	depend_classes = [EbuildToucher]
	eclass_depend_classes = [EbuildToucher]
	eclass_contents = ''

	def __init__(self, *args, **kwargs):
		EclassDependencyTestCase.__init__(self, *args, **kwargs)
		self.phase_funcs['src_compile'].extend((
			'pms-test-suite-%s || die' % self.dependant_ebuilds[0].pv,
			'pms-test-suite-%s || die' % self.dependant_ebuilds[1].pv
		))

class RDependInheritanceTest(EclassDependencyTestCase):
	""" RDEPEND variable inheritance test. """

	rdepend_classes = [EbuildToucher]
	eclass_rdepend_classes = [EbuildToucher]
	eclass_contents = ''

	def __init__(self, *args, **kwargs):
		EclassDependencyTestCase.__init__(self, *args, **kwargs)
		self.phase_funcs['pkg_postinst'].extend((
			'pms-test-suite-%s || die' % self.dependant_ebuilds[0].pv,
			'pms-test-suite-%s || die' % self.dependant_ebuilds[1].pv
		))

class PDependInheritanceTest(EclassDependencyTestCase):
	""" PDEPEND variable inheritance test. """

	pdepend_classes = [EbuildToucher]
	eclass_pdepend_classes = [EbuildToucher]
	eclass_contents = ''
