#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

"""
>>> l = load_library('test')
>>> l # doctest: +ELLIPSIS
<pmstestsuite.library.test.ExampleLibrary object at ...>
>>> t = [x for x in l][0]
>>> t # doctest: +ELLIPSIS
<pmstestsuite.library.test.random_test.RandomExampleTest object at ...>
>>> files = t.get_output_files()
>>> files # doctest: +ELLIPSIS
{'pms-test/random-example/random-example-0.ebuild': ...}
>>> t.ebuild_vars['DESCRIPTION']
'An absolutely random test'
>>> f = files.values()[0]
>>> str(f) # doctest: +ELLIPSIS
'# Copyright 1999...inherit pms-test...die...'
"""

from abc import abstractproperty
from gentoopm.util import ABCObject

from .case import TestCase

class TestLibrary(ABCObject):
	"""
	Base class for a test library.

	@ivar tests: cache of instantiated tests
	@type tests: list(L{TestCase})
	"""

	tests = None

	@abstractproperty
	def test_names(self):
		"""
		Names of test classes in the library, including a module path relative
		to the library root.

		@type: iterable(string)
		"""

	def __iter__(self):
		"""
		Iterate over all the tests in a test library, loading its modules
		as necessary.

		@return: an iterable over instantiated test cases
		@rtype: generator(L{TestCase})
		@raise ImportError: if submodule import fails.
		"""

		if self.tests is not None:
			# tests already instantiated
			for t in self.tests:
				yield t
		else:
			self.tests = []
			for t in self.test_names:
				# XXX: tests without a '.' in __init__?
				modname, clsname = t.rsplit('.', 1)
				modname = '%s.%s' % (self.modname, modname)
				mod = __import__(modname, fromlist=[clsname], globals=globals(), level=1)
				cls = getattr(mod, clsname)
				for i in cls.inst_all(thorough = self.thorough,
						undefined = self.undefined, short_name = t,
						dbus_hdlr = self.dbus_hdlr):
					self.tests.append(i)
					yield i

	def __len__(self):
		if self.tests is None:
			# pre-init, shouldn't be needed
			for t in self:
				pass

		return len(self.tests)

	def __init__(self, modname, dbus_hdlr, thorough = False, undefined = False):
		self.modname = modname
		self.dbus_hdlr = dbus_hdlr
		self.thorough = thorough
		self.undefined = undefined

	def limit_tests(self, limitations):
		if self.tests is not None:
			raise SystemError('TestLibrary.limit_tests() must be called before initiating tests.')

		ls = []
		for l in limitations:
			ls.append(l.split('.'))

		newnames = set()
		for t in self.test_names:
			ts = t.split('.')
			for l in ls:
				if ts[:len(l)] == l:
					newnames.add(t)

		self.test_names = newnames

	def get_common_files(self):
		""" Return common files necessary for the library (e.g. eclasses). """
		return {}

def load_library(name, **kwargs):
	"""
	Try to load a test library. Instiantiate the first TestLibrary subclass
	found there.

	@param name: the test library name
	@type name: string
	@param kwargs: keyword arguments to pass to the library constructor
	@return: an instantiated test library
	@rtype: L{TestLibrary}
	@raise ImportError: if module import fails
	@raise TypeError: if no matching class is found in the module
	"""

	mod = __import__(name, fromlist=['.'], globals=globals(), level=1)

	for k in dir(mod):
		modvar = getattr(mod, k)
		# the !issubclass() check is necessary to omit TestLibrary class
		# imported for the subclass
		try:
			if issubclass(modvar, TestLibrary) and \
					not issubclass(TestLibrary, modvar):
				cls = modvar
				break
		except TypeError:
			pass
	else:
		raise TypeError('Unable to find a TestLibrary subclass in %s'
				% name)

	return cls(name, **kwargs)
