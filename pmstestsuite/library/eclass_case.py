#	vim:fileencoding=utf-8
# (c) 2011-2012 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from abc import abstractproperty

from .case import EbuildTestCase

class EclassTestCase(EbuildTestCase):
	"""
	Test case using an eclass (and a set of per-EAPI ebuilds).

	The eclass inherit will be added to the ebuilds automatically.
	"""

	@abstractproperty
	def eclass_contents(self):
		"""
		The eclass contents.

		@type: string
		"""
		pass

	def __init__(self, *args, **kwargs):
		EbuildTestCase.__init__(self, *args, **kwargs)
		self.inherits.append(self.pn)

	def get_output_files(self):
		outf = EbuildTestCase.get_output_files(self)
		outf['eclass/%s.eclass' % self.pn] = self.eclass_contents
		return outf
