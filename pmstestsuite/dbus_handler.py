#	vim:fileencoding=utf-8
# (c) 2011-2012 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

import dbus, os, signal, subprocess, tempfile
from dbus.mainloop.glib import DBusGMainLoop

dbus_interface_name = 'org.gentoo.pmstestsuite'
dbus_bus_name = dbus_interface_name
dbus_object_prefix = '/org/gentoo/pmstestsuite'

dbus_config = """<?xml version="1.0"?>
<!DOCTYPE busconfig PUBLIC "-//freedesktop//DTD D-BUS Bus Configuration 1.0//EN"
	"http://www.freedesktop.org/standards/dbus/1.0/busconfig.dtd">

<busconfig>
	<type>session</type>
	<listen>unix:tmpdir=/tmp</listen>

	<user>%s</user>

	<policy context="default">
		<allow user="%s"/>

		<allow send_destination="*" eavesdrop="true"/>
		<allow eavesdrop="true"/>
		<allow own="*"/>
	</policy>
</busconfig>"""

class DBusHandler(object):
	""" A class handling all D-Bus interaction for PMS Test Suite. """

	def start_dbus(self, uid):
		tmpf = tempfile.NamedTemporaryFile('w')
		tmpf.write(dbus_config % (uid, os.getuid()))
		tmpf.flush()

		self.subp = subprocess.Popen(['dbus-daemon', '--nofork',
			'--config-file=%s' % tmpf.name, '--print-address'],
			stdout = subprocess.PIPE, close_fds = True)

		addr = self.subp.stdout.readline()
		tmpf.close()

		self.bus_address = addr.strip()

	def terminate(self):
		self.subp.terminate()

	def __init__(self, uid):
		""" Initialize DBusHandler. Add it to main GLib loop. """
		DBusGMainLoop(set_as_default=True)
		self.start_dbus(uid)
		os.environ['DBUS_SESSION_BUS_ADDRESS'] = self.bus_address
		self.bus = dbus.SessionBus()
		self.busname = dbus.service.BusName(dbus_bus_name, self.bus)
