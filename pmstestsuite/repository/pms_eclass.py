#	vim:fileencoding=utf-8
# (c) 2011-2012 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

import os.path

from ..dbus_handler import dbus_bus_name, dbus_object_prefix, \
		dbus_interface_name

eclass_contents = """# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EXPORT_FUNCTIONS pkg_setup

HOMEPAGE="http://www.gentoo.org/proj/en/qa/pms/pms-test-suite.xml"
SRC_URI=""

LICENSE="as-is"
SLOT=${PV}
IUSE=""

RESTRICT="mirror"
S=${WORKDIR}

pms-test_dbus_call() {
	local method=${1}
	shift

	PMS_TEST_DBUS_P=${P//-/_}
	dbus-send \\
		--session \\
		--print-reply \\
		--type=method_call \\
		--dest=%s \\
		%s/${PMS_TEST_DBUS_P} \\
		%s.${method} \\
		"${@}"
}

pms-test_dbus_append_result() {
	pms-test_dbus_call append_output \\
		string:"${*}" \\
		|| die 'Failed to append the test result using dbus'
}

pms-test_pkg_setup() {
	pms-test_dbus_call test_started
}
""" % (dbus_bus_name, dbus_object_prefix, dbus_interface_name)

def get_common_eclass_files():
	"""
	Return a file-dict for the eclass.

	@return: file-dict with the eclass contents
	@rtype: dict(string -> stringifiable)
	"""
	return {
		os.path.join('eclass', 'pms-test.eclass'): eclass_contents
	}
