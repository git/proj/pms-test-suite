#!/usr/bin/python
#	vim:fileencoding=utf-8
# (c) 2011 Michał Górny <mgorny@gentoo.org>
# Released under the terms of the 2-clause BSD license.

from distutils.core import setup, Command

import os.path, subprocess, sys

sys.path.insert(0, os.path.dirname(__file__))
try:
	from pmstestsuite import PV
except ImportError:
	PV = 'unknown'

class DocCommand(Command):
	description = 'create HTML docs'
	user_options = []

	def initialize_options(self):
		pass

	def finalize_options(self):
		pass

	def run(self):
		print('Creating API docs')
		subprocess.check_call(['epydoc', '--verbose', '--html',
			'--output', 'doc', 'pmstestsuite'])

class TestCommand(Command):
	description = 'run tests'
	user_options = []

	def initialize_options(self):
		pass

	def finalize_options(self):
		pass

	def run(self):
		import unittest, doctest

		tests = unittest.TestSuite()
		tests.addTests(doctest.DocTestSuite('pmstestsuite.cli'))
		tests.addTests(doctest.DocTestSuite('pmstestsuite.library'))
		tests.addTests(doctest.DocTestSuite('pmstestsuite.library.case'))
		tests.addTests(doctest.DocTestSuite('pmstestsuite.pm'))
		tests.addTests(doctest.DocTestSuite('pmstestsuite.pm.pkgcorepm'))
		tests.addTests(doctest.DocTestSuite('pmstestsuite.pm.portagepm'))
		tests.addTests(doctest.DocTestSuite('pmstestsuite.repository'))

		r = unittest.TextTestRunner()
		res = r.run(tests)
		sys.exit(0 if res.wasSuccessful() else 1)

setup(
		name = 'pms-test-suite',
		version = PV,
		author = 'Michał Górny',
		author_email = 'mgorny@gentoo.org',
		url = 'http://www.gentoo.org/proj/en/qa/pms/pms-test-suite.xml',

		packages = [
			'pmstestsuite',
			'pmstestsuite.library',
			'pmstestsuite.library.standard',
			'pmstestsuite.library.test',
			'pmstestsuite.output',
			'pmstestsuite.pm',
			'pmstestsuite.repository'
		],
		scripts = [
			'pms-tester'
		],

		classifiers = [
			'Development Status :: 4 - Beta',
			'Environment :: Console',
			'Intended Audience :: System Administrators',
			'License :: OSI Approved :: BSD License',
			'Operating System :: POSIX',
			'Programming Language :: Python',
			'Topic :: Software Development :: Quality Assurance',
			'Topic :: Software Development :: Testing',
			'Topic :: System :: Installation/Setup'
		],

		cmdclass = {
			'doc': DocCommand,
			'test': TestCommand
		}
)
